﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace IntegrationTesting.Services
{
    public interface ISerialService
    {
        public ObservableCollection<string> GetAvailiblePorts();
    }
}
