﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO.Ports;
using System.Threading;
using System.Collections.ObjectModel;

namespace IntegrationTesting.Services
{
    public class SerialService : ISerialService
    {
        static SerialPort _serialPort = new SerialPort();
        static Thread readThread = new Thread(Read);
        public SerialService()
        {
            _serialPort.BaudRate = 9600;
            _serialPort.Parity = Parity.None;
            _serialPort.StopBits = StopBits.One;
            _serialPort.DataBits = 8;
            //readThread.Start();
        }

        public void OpenPort(string portName)
        {
            if (_serialPort.IsOpen) _serialPort.Close();
            _serialPort.PortName = portName;
            _serialPort.Open();
        }

        public ObservableCollection<string> GetAvailiblePorts()
        {
            return new ObservableCollection<string>() {"New Port1", "New Port2" };
        }

        private static void Read()
        {
            while (true)
            {
                string message = _serialPort.ReadExisting();

                if (message.Length != 0)
                {
                    Console.WriteLine(message);
                }
            }
        }


    }
}
