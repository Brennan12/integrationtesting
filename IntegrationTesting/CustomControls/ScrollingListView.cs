﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Controls;

namespace IntegrationTesting.CustomControls
{
    public class ScrollingListView : ListBox
    {
        protected override void OnItemsChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems == null) return;

            if (e.NewItems.Count > 0)
            {
                int newItemCount = e.NewItems.Count;

                if (newItemCount > 0)
                    this.ScrollIntoView(e.NewItems[newItemCount - 1]);

                base.OnItemsChanged(e);
            }

        }
    }
}
