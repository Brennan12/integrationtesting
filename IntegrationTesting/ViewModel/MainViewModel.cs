﻿using IntegrationTesting.Services;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Windows.Input;

namespace IntegrationTesting.ViewModel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private ISerialService _serialService;
        public MainViewModel(ISerialService serialService)
        {
            _serialService = serialService;
            _printouts = new ObservableCollection<string>();
            _availiblePorts = new ObservableCollection<string>()
            {
                "Port1",
                "Port2",
                "Port3",
                "Port4"
            };
        }
        public ICommand ClearButtonPressed
        {
            get { return new DelegateCommand<object>(ClearFunction, ClearEval); }
        }

        private bool ClearEval(object arg)
        {
            return true;
        }

        private void ClearFunction(object obj)
        {
            Printouts.Add((DateTime.Now.ToString("HH:mm:ss.fff",CultureInfo.InvariantCulture)) + "->" +" Testing");
        }

        private bool _isDropDownOpen;
        public bool IsDropDownOpen
        {
            get { return _isDropDownOpen; }
            set {
                AvailiblePorts = _serialService.GetAvailiblePorts();
                _isDropDownOpen = value;
                OnPropertyChange(nameof(IsDropDownOpen));
            }
        }

        private string _selectedPort;
        public string SelectedPort
        {
            get { return _selectedPort; }
            set {
                _selectedPort = value;
                OnPropertyChange(nameof(SelectedPort));
            }
        }

        private ObservableCollection<string> _printouts;
        public ObservableCollection<string> Printouts
        {
            get { return _printouts; }
            set {
                _printouts = value;
                OnPropertyChange(nameof(Printouts));
            }
        }

        private ObservableCollection<string> _availiblePorts;
        public ObservableCollection<string> AvailiblePorts
        {
            get { return _availiblePorts; }
            set {
                _availiblePorts = value;
                OnPropertyChange(nameof(AvailiblePorts));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChange(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
