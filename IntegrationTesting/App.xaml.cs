﻿using IntegrationTesting.Services;
using IntegrationTesting.View;
using IntegrationTesting.ViewModel;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Unity;

namespace IntegrationTesting
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly ServiceProvider _serviceProvider;

        public App()
        {
            var serviceCollection = new ServiceCollection();

            serviceCollection.AddSingleton<ISerialService>(new SerialService());
            serviceCollection.AddSingleton<MainViewModel>();

            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        private void OnStartup(object sender, StartupEventArgs e)
        {
            var main = new Main();
            main.DataContext = _serviceProvider.GetService<MainViewModel>();
            main.Show();
        }
    }
}
